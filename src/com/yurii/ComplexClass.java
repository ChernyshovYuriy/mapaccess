package com.yurii;

/**
 * Created by yurii
 * At Intellij IDEA
 * On 29/10/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class ComplexClass {

    private Thread thread;
    private String string;
    private Integer integer;
    private Double aDouble;
    private SubClassA subClassA;

    public ComplexClass() {
        super();

        thread = new Thread(new RunnableImp(new SubClassA()), "ThreadName");
        thread.start();

        string = "This is String";
        integer = 123;
        aDouble = 123.456;
        subClassA = new SubClassA();
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }

    public SubClassA getSubClassA() {
        return subClassA;
    }

    public void setSubClassA(SubClassA subClassA) {
        this.subClassA = subClassA;
    }

    private static final class RunnableImp implements Runnable {

        private SubClassA subClassA;

        public RunnableImp(final SubClassA subClassA) {
            this.subClassA = subClassA;
        }

        @Override
        public void run() {
            subClassA.invoke();
        }
    }
}

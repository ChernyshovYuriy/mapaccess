package com.yurii;

/**
 * Created by yurii
 * At Intellij IDEA
 * On 29/10/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class SubClassA {

    private Thread thread;
    private String string;
    private Integer integer;
    private Double aDouble;

    public SubClassA() {
        super();
    }

    public void invoke() {
        //System.out.println("Invoked by:" + Thread.currentThread().getName());
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }
}

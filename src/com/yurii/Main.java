package com.yurii;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        final int size = 100;
        final Map<ComplexClass, ComplexClass> mapByObject = new HashMap<>();
        final Map<Integer, ComplexClass> mapById = new HashMap<>();
        final int[] hashIds = new int[size];
        final ComplexClass[] objects = new ComplexClass[size];

        ComplexClass complexClassA;
        ComplexClass complexClassB;
        long startTime;
        long timeByObject = 0;
        long timeById = 0;
        int hashId;
        int i;

        for (i = 0; i < size; i++) {
            complexClassA = new ComplexClass();
            complexClassB = new ComplexClass();
            hashId = complexClassA.hashCode();
            hashIds[i] = hashId;
            objects[i] = complexClassA;

            startTime = System.nanoTime();
            mapByObject.put(complexClassA, complexClassB);
            timeByObject += (System.nanoTime() - startTime);

            startTime = System.nanoTime();
            mapById.put(hashId, complexClassB);
            timeById += (System.nanoTime() - startTime);
        }

        System.out.println("Put time by Object:" + ((timeByObject / size) / 1000) + " ms");
        System.out.println("Put time by Id    :" + ((timeById / size) / 1000) + " ms");

        for (i = 0; i < size; i++) {
            hashId = hashIds[i];
            complexClassA = objects[i];

            startTime = System.nanoTime();
            mapByObject.get(complexClassA);
            timeByObject += (System.nanoTime() - startTime);

            startTime = System.nanoTime();
            mapById.get(hashId);
            timeById += (System.nanoTime() - startTime);
        }

        System.out.println("Get time by Object:" + ((timeByObject / size) / 1000) + " ms");
        System.out.println("Get time by Id    :" + ((timeById / size) / 1000) + " ms");

        for (i = 0; i < size; i++) {
            hashId = hashIds[i];
            complexClassA = objects[i];

            startTime = System.nanoTime();
            mapByObject.remove(complexClassA);
            timeByObject += (System.nanoTime() - startTime);

            startTime = System.nanoTime();
            mapById.remove(hashId);
            timeById += (System.nanoTime() - startTime);
        }

        System.out.println("Remove time by Object:" + ((timeByObject / size) / 1000) + " ms");
        System.out.println("Remove time by Id    :" + ((timeById / size) / 1000) + " ms");
    }
}
